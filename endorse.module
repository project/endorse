<?php

include_once('endorse.features.inc');

/**
 * @file
 * TODO: Enter file description here.
 */

/**
 * Permissions
 */
function endorse_perm() {
  return array('access endorse', 'administer endorse');
}


/**
 * Implementation of hook_menu
 */
function endorse_menu() {
  $items = array();
  $items['endorse'] = array(
    'title'            => 'Endorse',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('endorse_page_form'),
    'access arguments' => array('access endorse'),
    'type'             => MENU_NORMAL_ITEM
  );
  $items['admin/settings/endorse'] = array(
    'description'      => t('Configure settings for endorse module.'),
    'title'            => 'Endorse',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('endorse_admin_settings'),
    'access arguments' => array('administer endorse'),
    'type'             => MENU_NORMAL_ITEM
  );
  return $items;
}


/**
 * Administration Page
 */
function endorse_admin_settings() {
  // Administrative Options
  $form['endorse_title'] = array(
    '#type' => 'textfield',
    '#title' => 'Endorse page title',
    '#default_value' => variable_get('endorse_title', t('Endorse !sitename', array('!sitename' => variable_get('site_name', 'Drupal')))),
  );
  $form['endorse_header'] = array(
    '#type' => 'textarea',
    '#title' => 'Endorse page header text',
    '#default_value' => variable_get('endorse_header', t('Please tell us why you support !sitename:', array('!sitename' => variable_get('site_name', 'Drupal')))),
    '#cols' => 60,
    '#rows' => 5,
  );
  $form['endorse_header'] = array(
    '#type' => 'textarea',
    '#title' => 'Endorse page header text',
    '#default_value' => variable_get('endorse_footer', t('')),
    '#cols' => 60,
    '#rows' => 5,
  );
  $form['endorse_redirect'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirect path'),
    '#size' => 40,
    '#default_value' => variable_get('endorse_redirect', '<front>'),
    '#description' => t('Set the path to redirect users to uopn complting the form.  Should not contain a leading or trailing slash.  Use <front> for the homepage.'),
  );
  $form['endorse_email'] = array(
    '#type' => 'checkbox',
    '#title' => 'Send email notifications',
    '#default_value' => variable_get('endorse_email', FALSE),
  );
  $form['endorse_email_destination'] = array(
    '#type' => 'textfield',
    '#title' => 'Email notification recipient',
    '#default_value' => variable_get('endorse_email_destination', variable_get('site_mail', '')),
  );

  return system_settings_form($form);
}


/**
 * Implementation of hook_node_info().
 */
function endorse_node_info() {
  return array(
    'endorse' => array(
      'name' => 'Endorsement',
      'module' => 'endorse',
      'description' => '',
      'help' => variable_get('endorse_help', ''),
      'has_title' => 1,
      'title_label' => 'Name, Title and Organization',
      'has_body' => 1,
      'body_label' => 'Endorsement',
    ),
  );
}


function endorse_form(&$node, $form_state) {
  // The site admin can decide if this node type has a title and body, and how
  // the fields should be labeled. We need to load these settings so we can
  // build the node form correctly.
  $type = node_get_types('type', $node);

  if ($type->has_body) {
    // In Drupal 6, we can use node_body_field() to get the body and filter
    // elements. This replaces the old textarea + filter_form() method of
    // setting this up. It will also ensure the teaser splitter gets set up
    // properly.
    $form['body_field'] = node_body_field($node, $type->body_label, $type->min_word_count);
    $form['body_field']['#weight'] = -5;
  }

  if ($type->has_title) {
    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => check_plain($type->title_label),
      '#required' => TRUE,
      '#default_value' => $node->title,
      '#weight' => -4
    );
  }

  return $form;
}


/**
 * Implementation of hook_form().
 */
function endorse_page_form(&$form_state) {
  drupal_set_title(variable_get('endorse_title', t('Endorse !sitename', array('!sitename' => variable_get('site_name', 'Drupal')))));

  $form['header'] = array(
    '#type' => 'markup',
    '#value' => variable_get('endorse_header', t('Please tell us why you support !sitename:', array('!sitename' => variable_get('site_name', 'Drupal')))),
  );
  $form['first_name'] = array(
    '#type' => 'textfield',
    '#title' => 'First name',
    '#required' => TRUE,
  );
  $form['last_name'] = array(
    '#type' => 'textfield',
    '#title' => 'Last name',
    '#required' => TRUE,
  );
  $form['job_title'] = array(
    '#type' => 'textfield',
    '#title' => 'Title',
  );
  $form['organization'] = array(
    '#type' => 'textfield',
    '#title' => 'Organization',
  );
  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => 'Email Address',
    '#required' => TRUE,
  );
  $form['zip'] = array(
    '#type' => 'textfield',
    '#title' => 'Zip Code',
    '#required' => TRUE,
  );
  $form['body'] = array(
    '#type' => 'textarea',
    '#title' => 'Endorsement',
    '#cols' => 60,
    '#rows' => 10,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Endorse !sitename', array('!sitename' => variable_get('site_name', 'Drupal'))),
  );
  $form['footer'] = array(
    '#type' => 'markup',
    '#value' => variable_get('endorse_footer', t('')),
  );

  return $form;
}


/**
 * Implementation of hook_form_validate().
 */
function endorse_page_form_validate($form, &$form_state) {
  if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $form_state['values']['email'])) {
    form_set_error('email', t('You must enter a valid email address.'));
  }
  if ((!preg_match("/^([0-9]{5})(-[0-9]{4})?$/i", $form_state['values']['zip'])) || ($form_state['values']['zip'] == '00000') || ($form_state['values']['zip'] == '00000-0000')) {
    form_set_error('zip', t('The zip code must be in the format xxxxx or xxxxx-xxxx.'));
  }
}


/**
 * Implementation of hook_form_submit().
 */
function endorse_page_form_submit($form, &$form_state) {
  $node = array();

  // Set the node title
  $node['title'] = $form_state['values']['first_name'] . ' ' . $form_state['values']['last_name'];

  // Append job title
  if (!empty($form_state['values']['job_title'])) {
    $node['title'] .= ' - ' . $form_state['values']['job_title'];
  }

  // Append Organization
  if (!empty($form_state['values']['organization'])) {
    $node['title'] .= ', ' . $form_state['values']['organization'];
  }

  // Set the node body, status and type
  $node['body'] = check_markup($form_state['values']['body'], 1);
  $node['status'] = 0;
  $node['type'] = 'endorse';

  $node = node_submit($node);
  node_save($node);

  // CRMAPI hook - saves data to default enabled CRM
  if (module_exists('crmapi') && !empty($form_state['values']['first_name'])) {
    $contact['first_name'] = $form_state['values']['first_name'];
    $contact['last_name'] = $form_state['values']['last_name'];
    $contact['mail_name'] = $form_state['values']['first_name'] . ' ' . $form_state['values']['last_name'];
    $contact['occupation'] = $form_state['values']['job_title'];
    $contact['organization'] = $form_state['values']['organization'];
    $contact['email'] = $form_state['values']['email'];
    $contact['zip'] = $form_state['values']['zip'];

    $contact_id = crmapi_contact_save($contact);

    $activity_params = array(
      'contact_id' => $contact_id,
      'activity_id' => 'OLEndorse',
      'activity_type' => 'E',
      'level' => '',
      'flag' => '',
      'description' => substr($form_state['values']['body'], 0, 100)
    );
    crmapi_activity_save($activity_params);
  }

  $subject = 'Endorsement received from ' . $node->title;
  $body = "Received the following form submission:\r\n\r\n";
  foreach ($form_state['values'] as $key => $value) {
    $body .= "$key = $value\r\n";
  }
  $body .= "\r\nView the post at " . url('node/'.$node->nid, array('absolute' => TRUE));

  drupal_mail('endorse', 'endorse-admin', variable_get('endorse_email_destination', variable_get('site_mail', '')), language_default(), array('subject' => $subject, 'body' => $body), variable_get('site_mail', ''));

  drupal_set_message(t('Thank you for endorsing !sitename', array('!sitename' => variable_get('site_name', 'Drupal'))));
  $form_state['redirect'] = variable_get('endorse_redirect', '<front>');
}


/**
 * Implementation of hook_mail().
 *
 * Constructs the email notification message when the site is out of date.
 *
 * @param $key
 *   Unique key to indicate what message to build, always 'forward_page'.
 * @param $message
 *   Reference to the message array being built.
 * @param $params
 *   Array of parameters to indicate what text to include in the message body.
 *
 * @see drupal_mail();
 * @see _update_cron_notify();
 * @see _update_message_text();
 */
function endorse_mail($key, &$message, $params) {
  $message['subject'] .= $params['subject'];
  $message['body'][] = $params['body'];
  $message['headers']['MIME-Version'] = '1.0';
  $message['headers']['Content-Type'] = 'text/plain; charset=utf-8';
}


/**
 * Implementation of hook_theme_registry_alter().
 */
function endorse_theme_registry_alter(&$theme_registry) {
  // Shove our own node template in here to flip the title and body around.
  $theme_registry['node']['theme paths'][] = drupal_get_path('module', 'endorse');
}